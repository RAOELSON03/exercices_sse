const express = require('express');
const SSE = require('sse-nodejs');
const app = express();
const PORT = process.env.PORT || 3000;
const traitements = require('./routes/traitements');

app.use(express.json());
let data = "";
app.get('/', (req, res) => {
    res.sendFile(__dirname+ '/index.html');
});

app.get('/server', function (req,res) {
    let app = SSE(res);    
    app.sendEvent('romain', function () {
        return data;
    },1000);
 
    app.disconnect(function () {
        console.log("disconnected");
    });
 
});

app.post('/api', (req, res) => {
    const body = req.body.nombre;
    data = (traitements.getRoman(body));
    res.json('ok');
});


app.listen(PORT, () => {
    console.log(`server is running on port : ${PORT}`);
})